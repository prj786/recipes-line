const path = require('path');

const bodyParser = require('body-parser');
const express = require('express');
const app = express();

const mainRoutes = require('./routes/main');
const errorRoutes = require('./controllers/errors');

app.set('view engine', 'pug');
app.set('views', 'views');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(mainRoutes);
app.use(errorRoutes.notFound);

console.log('Port:', 5000);

app.listen(5000);

module.exports = {
    mainPath: path.dirname(require.main.filename)
}