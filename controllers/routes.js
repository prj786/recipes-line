const mainPath = require('../index');

module.exports = {
    getMainPage: (req, res) => {
        res.render('main', { title: 'Recipes Line', user: false, userName: 'LightDragon2021' });
    },
    getLoginPage: (req, res) => {
        res.render('auth', { title: 'Log in your account', login: true });
    },
    getRegisterPage: (req, res) => {
        res.render('auth', { title: 'Create new account', login: false });
    },
    getItemInfoPage: (req, res) => {
        const item = { name: req.params.itemInfo };
        res.render('recipe', { item: item, title: item.name })
    }
}