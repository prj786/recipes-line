const express = require('express');
const router = express.Router();

const routeController = require('../controllers/routes');

router.get('/', routeController.getMainPage);
router.get('/login', routeController.getLoginPage);
router.get('/register', routeController.getRegisterPage);
router.get('/recipe/:itemInfo', routeController.getItemInfoPage);

module.exports = router;